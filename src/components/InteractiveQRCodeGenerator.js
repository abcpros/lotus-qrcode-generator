import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import WebFont from "webfontloader";
import { Box, Button, Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
import QRCodeForm from "components/QRCodeForm";
import QRCodeGenerator from "components/QRCodeGenerator";
import downloadQRCodeAsPNG from "utils/downloadQRCodeAsPNG";
import {
  DOWNLOAD_QRCODE_LABEL,
  DEFAULT_FONT_FAMILY,
  GENERATED_QRCODE_GRAPHICS_SVG_ELEMENT_ID,
  FONT_FAMILY_SANS_SERIF,
  FONT_FAMILY_SERIF,
  FONT_FAMILY_MONOSPACE,
  FONT_FAMILY_HANDWRITING,
} from "utils/constants";

// default svg template
import defaultTemplate from "assets/templates/template_1.svg";

// default logo file
import defaultLogo from "assets/scanme.png";

// MUI specific way of creating css styles
const useStyles = makeStyles((theme) => ({
  center: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10px",
    marginBottom: "20px",
    minHeight: "40vh",
  },
}));

// InteractiveQRCodeGenerator Component
const InteractiveQRCodeGenerator = () => {
  const params = useParams();
  const [address, setAddress] = useState(params.address || "");
  const [text, setText] = useState(params.name || "");
  const [font, setFont] = useState(DEFAULT_FONT_FAMILY);
  const [template, setTemplate] = useState(defaultTemplate);
  const [logoURL, setLogoURL] = useState(defaultLogo);
  const classes = useStyles();

  // Preload the all the available fonts for the text in svg graphics
  // To avoid FOUT
  useEffect(() => {
    WebFont.load({
      custom: {
        families: [
          FONT_FAMILY_SANS_SERIF,
          FONT_FAMILY_SERIF,
          FONT_FAMILY_MONOSPACE,
          FONT_FAMILY_HANDWRITING,
        ],
      },
    });
  }, []);

  // Event handlers
  const handleAddressChange = (newAddress) => {
    setAddress(newAddress);
  };

  const handleTextChange = (newText) => {
    setText(newText);
  };

  const handleFontChange = (newFont) => {
    setFont(newFont);
  };

  const handleTemplateChange = (newTemplate) => {
    setTemplate(newTemplate);
  };

  const handleLogoChange = (newFile) => {
    if (!newFile) {
      setLogoURL(defaultLogo);
      return;
    }
    const URL = window.URL || window.webkitURL || window;
    setLogoURL(URL.createObjectURL(newFile));
  };

  return (
    <Container maxWidth="sm">
      <Box className={classes.center}>
        <QRCodeGenerator
          address={address}
          text={text}
          font={font}
          template={template}
          logoURL={logoURL}
        ></QRCodeGenerator>
      </Box>
      <QRCodeForm
        address={address}
        text={text}
        handleAddressChange={handleAddressChange}
        handleTextChange={handleTextChange}
        handleFontChange={handleFontChange}
        handleTemplateChange={handleTemplateChange}
        handleLogoChange={handleLogoChange}
      />
      <Box mt={3} mb={3}>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() =>
            downloadQRCodeAsPNG(GENERATED_QRCODE_GRAPHICS_SVG_ELEMENT_ID, font)
          }
        >
          {DOWNLOAD_QRCODE_LABEL}
        </Button>
      </Box>
    </Container>
  );
};

export default InteractiveQRCodeGenerator;
