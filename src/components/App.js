import { createTheme, ThemeProvider } from "@mui/material";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import InteractiveQRCodeGenerator from "components/InteractiveQRCodeGenerator";
import StaticQRCodeGenerator from "components/StaticQRCodeGenerator";

// Override the primary color of Material UI default theme
const theme = createTheme({
  palette: {
    primary: {
      light: "#7e4ca0",
      main: "#4f2171",
      dark: "#220045",
      contrastText: "#ffffff",
    },
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route path="/embed/:address/:name">
            <StaticQRCodeGenerator />
          </Route>
          <Route path="/:address?/:name?">
            <InteractiveQRCodeGenerator />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

export default App;
