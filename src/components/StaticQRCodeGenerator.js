import React, { useEffect } from "react";
import WebFont from "webfontloader";
import { useParams } from "react-router-dom";
import { Box, Button, Container } from "@mui/material";
import { SaveAlt as SaveAltIcon, Edit as EditIcon } from "@mui/icons-material";
import { makeStyles } from "@mui/styles";
import QRCodeGenerator from "components/QRCodeGenerator";
import downloadQRCodeAsPNG from "utils/downloadQRCodeAsPNG";
import {
  SAVE_LABEL,
  EDIT_LABEL,
  DEFAULT_FONT_FAMILY,
  GENERATED_QRCODE_GRAPHICS_SVG_ELEMENT_ID,
} from "utils/constants";

// default svg template
import defaultTemplate from "assets/templates/template_1.svg";

// default logo file
import defaultLogo from "assets/scanme.png";

// MUI specific way of creating css styles
const useStyles = makeStyles((theme) => ({
  center: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  cta: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "8px",
  },
}));

// StaticQRCodeGenerator Component
const StaticQRCodeGenerator = () => {
  const { address, name } = useParams();
  const classes = useStyles();

  // Preload the default font
  useEffect(() => {
    WebFont.load({
      custom: {
        families: [DEFAULT_FONT_FAMILY],
      },
    });
  }, []);

  return (
    <Container maxWidth="sm">
      <Box className={classes.center}>
        <QRCodeGenerator
          address={address}
          text={name}
          font={DEFAULT_FONT_FAMILY}
          template={defaultTemplate}
          logoURL={defaultLogo}
        ></QRCodeGenerator>
      </Box>
      <Box className={classes.cta}>
        <Button
          startIcon={<SaveAltIcon />}
          size="small"
          variant="outlined"
          onClick={() =>
            downloadQRCodeAsPNG(
              GENERATED_QRCODE_GRAPHICS_SVG_ELEMENT_ID,
              DEFAULT_FONT_FAMILY
            )
          }
          sx={{
            marginRight: "8px",
          }}
        >
          {SAVE_LABEL}
        </Button>
        <Button
          size="small"
          variant="outlined"
          startIcon={<EditIcon />}
          href={`${document.location.origin}/${address}/${name}`}
          target="_blank"
        >
          {EDIT_LABEL}
        </Button>
      </Box>
    </Container>
  );
};

export default StaticQRCodeGenerator;
