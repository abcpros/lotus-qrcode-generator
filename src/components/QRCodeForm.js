// TODO:
// 1. Instead of label, it should show thumbnail for each style / template

import React from "react";
import {
  Box,
  TextField,
  Typography,
  useMediaQuery,
  ToggleButtonGroup,
  ToggleButton,
  Grid,
  FormControl,
  NativeSelect,
  InputLabel,
} from "@mui/material";
import UploadImageButton from "components/UploadImageButton";

import {
  INPUT_ADDRESS_LABEL,
  INPUT_ADDRESS_PLACEHOLDER,
  INPUT_TEXT_LABEL,
  INPUT_TEXT_PLACEHOLDER,
  INPUT_UPLOAD_LOGO_LABEL,
  INPUT_STYLES_LABEL,
  DEFAULT_FONT_FAMILY,
  FONT_LABEL_SERIF,
  FONT_FAMILY_SERIF,
  FONT_LABEL_SANS_SERIF,
  FONT_FAMILY_SANS_SERIF,
  FONT_LABEL_MONOSPACE,
  FONT_FAMILY_MONOSPACE,
  FONT_LABEL_HANDWRITING,
  FONT_FAMILY_HANDWRITING,
} from "utils/constants";

// svg templates
import template1 from "assets/templates/template_1.svg";
import template2 from "assets/templates/template_2.svg";
import template3 from "assets/templates/template_3.svg";
import template4 from "assets/templates/template_4.svg";

// font options
const fontOptions = [
  {
    label: FONT_LABEL_SERIF,
    family: FONT_FAMILY_SERIF,
  },
  {
    label: FONT_LABEL_SANS_SERIF,
    family: FONT_FAMILY_SANS_SERIF,
  },
  {
    label: FONT_LABEL_MONOSPACE,
    family: FONT_FAMILY_MONOSPACE,
  },
  {
    label: FONT_LABEL_HANDWRITING,
    family: FONT_FAMILY_HANDWRITING,
  },
];

// style options
const styleOptions = [
  {
    label: "Top",
    template: template1,
  },
  {
    label: "Bottom",
    template: template2,
  },
  {
    label: "Left",
    template: template3,
  },
  {
    label: "Right",
    template: template4,
  },
];

// QRCodeForm Component
const QRCodeForm = (props) => {
  const {
    address,
    text,
    font,
    template,
    handleAddressChange,
    handleTemplateChange,
    handleTextChange,
    handleFontChange,
    handleLogoChange,
  } = props;

  // check the width of the viewport
  const isMD = useMediaQuery((theme) => theme.breakpoints.up("md"));

  // handle template change event
  const onTemplateChange = (event, newTemplate) => {
    if (newTemplate) {
      handleTemplateChange(newTemplate);
    }
  };

  return (
    <Box>
      {/* Address Input */}
      <Box mt={{ sm: 0, md: 1 }}>
        <TextField
          fullWidth
          variant="standard"
          label={INPUT_ADDRESS_LABEL}
          placeholder={INPUT_ADDRESS_PLACEHOLDER}
          name="address"
          value={address}
          onChange={(event) => handleAddressChange(event.target.value)}
        />
      </Box>

      {/* Text Input */}
      <Box mt={{ sm: 0, md: 1 }}>
        <Grid container spacing={1}>
          <Grid item xs={8}>
            <TextField
              fullWidth
              variant="standard"
              label={INPUT_TEXT_LABEL}
              placeholder={INPUT_TEXT_PLACEHOLDER}
              name="text"
              value={text}
              onChange={(event) => handleTextChange(event.target.value)}
            />
          </Grid>
          <Grid item xs={4}>
            <FormControl fullWidth>
              <InputLabel variant="standard" htmlFor="font-selection">
                Font
              </InputLabel>
              <NativeSelect
                defaultValue={font || DEFAULT_FONT_FAMILY}
                inputProps={{
                  name: "font",
                  id: "font-selection",
                  onChange(event) {
                    handleFontChange(event.target.value);
                  },
                }}
              >
                {fontOptions.map((opt) => (
                  <option key={opt.label} value={opt.family}>
                    {opt.label}
                  </option>
                ))}
              </NativeSelect>
            </FormControl>
          </Grid>
        </Grid>
      </Box>

      {/* Logo Upload */}
      <Box mt={3}>
        <Typography color="textSecondary" component="span">
          {INPUT_UPLOAD_LOGO_LABEL}
        </Typography>
        <UploadImageButton onChange={handleLogoChange} />
      </Box>

      {/* Templates */}
      <Box mt={3}>
        <Typography color="textSecondary">{INPUT_STYLES_LABEL}</Typography>
        <Box mt={1}>
          <ToggleButtonGroup
            exclusive
            value={template}
            onChange={onTemplateChange}
            aria-label="layout options"
            size={isMD ? "medium" : "small"}
          >
            {styleOptions.map((opt) => (
              <ToggleButton key={opt.label} value={opt.template}>
                {opt.label}
              </ToggleButton>
            ))}
          </ToggleButtonGroup>
        </Box>
      </Box>
    </Box>
  );
};

export default QRCodeForm;
