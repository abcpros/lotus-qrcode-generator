import React, { useState } from "react";
import { makeStyles } from "@mui/styles";
import { Image } from "@mui/icons-material";
import { Typography, IconButton } from "@mui/material";

import { ERR_MSG_INVALID_IMAGE_TYPE } from "utils/constants";

// Injecting styles with Material UI
const useStyles = makeStyles((theme) => ({
  hideFileInput: {
    display: "none",
  },
}));

// File Upload Button
function UploadImageButton(props) {
  const classes = useStyles();
  const [uploadedFile, setUploadedFile] = useState(null);
  const [errorMsg, setErrroMsg] = useState("");

  // check for correct image file type
  const isValidImageFileType = (file) => {
    // https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
    const fileTypes = [
      "image/apng",
      "image/bmp",
      "image/gif",
      "image/jpeg",
      "image/pjpeg",
      "image/png",
      "image/svg+xml",
      "image/tiff",
      "image/webp",
      "image/x-icon",
    ];

    return fileTypes.includes(file.type);
  };

  // handle file input change event
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      if (!isValidImageFileType(file)) {
        setErrroMsg(`${file.name} - ${ERR_MSG_INVALID_IMAGE_TYPE}`);
        return;
      }
      setUploadedFile(file);
      setErrroMsg("");
      props.onChange(file);
    }
  };

  // display the file name or error message
  let desc = "";
  if (errorMsg) {
    desc = errorMsg;
  } else {
    desc = uploadedFile ? uploadedFile.name : "";
  }

  return (
    <>
      <input
        className={classes.hideFileInput}
        type="file"
        accept="image/*"
        name="upload-logo-btn"
        id="upload-logo-btn"
        onChange={handleFileChange}
      />
      <label htmlFor="upload-logo-btn">
        <IconButton color="primary" aria-label="upload logo" component="span">
          <Image />
        </IconButton>
        <Typography component="span" color={errorMsg ? "error" : ""}>
          {desc}
        </Typography>
      </label>
    </>
  );
}

export default UploadImageButton;
