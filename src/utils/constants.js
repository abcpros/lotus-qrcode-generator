// FORM LABELS
export const INPUT_ADDRESS_LABEL = "Address";
export const INPUT_ADDRESS_PLACEHOLDER = "Your wallet address";
export const INPUT_TEXT_LABEL = "Text";
export const INPUT_TEXT_PLACEHOLDER = "Accepted Here, Give, Tip Us, etc.";
export const INPUT_UPLOAD_LOGO_LABEL = "Upload Your Logo";
export const INPUT_STYLES_LABEL = "Styles";
export const DOWNLOAD_QRCODE_LABEL = "Save As Image";
export const DOWNLOAD_QRCODE_SHORT_LABEL = "Download";
export const CUSTOMIZE_QRCODE_LABEL = "Customize";

// General Labels
export const SAVE_LABEL = "Save";
export const EDIT_LABEL = "Edit";

// Message
export const ERR_MSG_INVALID_IMAGE_TYPE = "Invalid image file type";

// SVG, HTML elements names
export const GENERATED_QRCODE_GRAPHICS_SVG_ELEMENT_ID =
  "generated_qrcode_graphics_svg_element_id";

// Fonts
export const FONT_LABEL_SERIF = "Serif";
export const FONT_FAMILY_SERIF = "Serif For QR";
export const FONT_LABEL_SANS_SERIF = "Sans Serif";
export const FONT_FAMILY_SANS_SERIF = "Sans Serif For QR";
export const FONT_LABEL_MONOSPACE = "Monospace";
export const FONT_FAMILY_MONOSPACE = "Monospace For QR";
export const FONT_LABEL_HANDWRITING = "Handwriting";
export const FONT_FAMILY_HANDWRITING = "Handwriting For QR";

// Default values
export const DEFAULT_FONT_FAMILY = FONT_FAMILY_SANS_SERIF;
