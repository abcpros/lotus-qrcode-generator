import { fetchFontFileAsDataURL } from "utils/dataURLFunctions";

// convert the QRCODE from SVG to PNG and start download
const downloadQRCodeAsPNG = async (svgElId, fontFamily) => {
  // To convert a svg element to image (PNG, JPG, etc):
  // - we first need to draw that svg element on a canvas
  // - we then export the content of the canvas to image
  // Note:
  // For the svg element to be fully rendered on the canvas, all external elements need to be embedded.
  // - Font needs to be embedded
  // - Images need to be embedded
  //
  // The process of converting the QRCOde SVG Template to Image
  // 1. Get a reference to the template svg element
  // 2. Get a reference to the logo image within the svg element
  // 3. Convert the that logo from linking to an external file to embedded data
  // 4. Load the seleted font file and embed it to the svg
  // 5. Draw the svg element on a canvas
  // 6. Export the canvas content to image

  // get a reference to the svg element
  const svg = document.getElementById(svgElId);
  if (!svg) {
    console.error("Cannot find the generated svg element");
    alert("Something when wrong, cannot find the generated graphics");
    return;
  }

  // get the font file as DataURL
  let fontDataURL = await fetchFontFileAsDataURL(fontFamily);
  // some server may repsond with "Content-Type" set to
  // "font/ttf; charset=utf-8" instead of just "font/ttf"
  // Firefox does not like it when "charset=uft-8" is included
  // So we make sure it is not included
  fontDataURL = fontDataURL.replace(" charset=utf-8;", "");

  // insert the Font DataURL to the style element of svg
  const svgStyleEl = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "style"
  );
  svgStyleEl.textContent = `
     @font-face {
       font-family: "${fontFamily}";
       font-style: normal;
       font-weight: bold;
       src: url(${fontDataURL}) format("truetype");
     }
     
     text {
       font-family: "${fontFamily}";
       font-style: normal;
       font-weight: bold;
       font-size:16px;
       fill: #ffffff;
       line-height:1.25;
     }
   `;
  svg.insertAdjacentElement("afterbegin", svgStyleEl);

  // get a reference to the logo image within the svg element
  const logoImageEl = svg.getElementsByTagName("image")[0];
  const logoImageUrl = logoImageEl.getAttribute("xlink:href");
  // convert the logo image to Data URL by drawing it on a canvas
  const logoImg = new Image();
  logoImg.onload = () => {
    const canvas = document.createElement("canvas");
    canvas.width = logoImg.width;
    canvas.height = logoImg.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(logoImg, 0, 0, canvas.width, canvas.height);
    const logoPng = canvas.toDataURL("image/png");
    // replace the external file link with embeded data
    // "xlink:href" is deprecated, use "href" instead
    logoImageEl.setAttribute("xlink:href", logoPng);

    // We cannot draw a svg element directly to a canvas
    // The svg element needs to be drawn as a image
    //
    // Create a Data URL from the svg so we can set it as the src of Image element
    const svgString = new XMLSerializer().serializeToString(svg);
    const svgBlob = new Blob([svgString], {
      type: "image/svg+xml",
    });
    const URL = window.URL || window.webkitURL || window;
    const url = URL.createObjectURL(svgBlob);

    // create an Image element so we can draw it on the canvas
    const img = new Image();
    img.onload = () => {
      // draw the image on a canvas
      // convert the canvas to png
      const canvas = document.createElement("canvas");
      canvas.width = svg.getAttribute("width");
      canvas.height = svg.getAttribute("height");
      const ctx = canvas.getContext("2d");
      // There is a bug in Webkit that prevents the embedded image from being drawn correctly
      // See https://bugs.webkit.org/show_bug.cgi?id=39059
      // The bug is caused by the embedded image not being converted from Data URL back to image
      // in time for the rendering on canvas.
      // This only happens on MacOS, not Windows or Linux
      // SetTimeout delay here, hopefully the image is converted / decoded in time
      setTimeout(() => {
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        const png = canvas.toDataURL("image/png");
        // create a link and download the image
        const a = document.createElement("a");
        a.download = "QRCode.png";
        a.href = png;
        a.click();

        // allow browser to clean up when neccessary
        URL.revokeObjectURL(url);
      }, 50);
    };
    img.src = url;
  };
  logoImg.src = logoImageUrl;
};

export default downloadQRCodeAsPNG;
