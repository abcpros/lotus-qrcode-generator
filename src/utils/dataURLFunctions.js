import {
  FONT_FAMILY_SANS_SERIF,
  FONT_FAMILY_SERIF,
  FONT_FAMILY_MONOSPACE,
  FONT_FAMILY_HANDWRITING,
} from "utils/constants";

// font files
import sansSerifFont from "assets/fonts/Roboto-Bold.ttf";
import serifFont from "assets/fonts/RobotoSlab-Bold.ttf";
import monospaceFont from "assets/fonts/RobotoMono-Bold.ttf";
import handwritingFont from "assets/fonts/DancingScript-Bold.ttf";

// Fetch a font file and turn it into Data URL
const fetchFontFileAsDataURL = async (fontFamily) => {
  let fontFile = "";
  switch (fontFamily) {
    case FONT_FAMILY_SANS_SERIF:
      fontFile = sansSerifFont;
      break;
    case FONT_FAMILY_SERIF:
      fontFile = serifFont;
      break;
    case FONT_FAMILY_MONOSPACE:
      fontFile = monospaceFont;
      break;
    case FONT_FAMILY_HANDWRITING:
      fontFile = handwritingFont;
      break;
    default:
      fontFile = sansSerifFont;
  }
  const response = await fetch(fontFile);
  const fontBlob = await response.blob();
  const fileReader = new FileReader();
  await new Promise((resolve, reject) => {
    fileReader.onload = resolve;
    fileReader.onError = reject;
    fileReader.readAsDataURL(fontBlob);
  });
  return fileReader.result;
};

export { fetchFontFileAsDataURL };
